# Oh My Zsh + Powerlevel10k
My zsh and Powerlevel10k config files for iTerm2.

## Instructions

Do the instructions below then execute

	ln -s $(pwd)/.zshrc ~/.zshrc
	ln -s $(pwd)/.p10k.zsh ~/.p10k.zsh
	ln -s $(pwd)/.fzf.zsh ~/.fzf.zsh

### zsh

1. Install [Oh My Zsh](https://ohmyz.sh/#install).
1. Install [Powerlevel10k](https://github.com/romkatv/powerlevel10k).
1. (Optional) Install [`zsh-syntax-highlighting`](https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/INSTALL.md) using the plugin manager method.

### fzf

1. Install [fzf](https://github.com/junegunn/fzf)
